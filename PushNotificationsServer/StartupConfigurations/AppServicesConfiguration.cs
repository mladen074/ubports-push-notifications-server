﻿using Microsoft.Extensions.DependencyInjection;
using PushNotificationsServer.Interfaces;
using PushNotificationsServer.Services;
using System;
using RabbitMQ.Client;

namespace PushNotificationsServer.StartupConfigurations
{
	public static class AppServicesConfiguration
	{
		public static void ConfigureAppServices(this IServiceCollection services)
		{
			if (services == null) throw new ArgumentNullException(nameof(services));

			services.AddTransient<IQueueService, RabbitMqService>();
			services.AddTransient<IConnectionFactory, ConnectionFactory>();
		}
	}
}
