﻿using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PushNotificationsServer.Infrastructure.ErrorHandling
{
	public class DefaultExceptionHandler : IExceptionHandler
	{
		public bool CanHandle(Type type) => true;
		public bool StopProcessingOtherHandlers() => true;

		public async Task HandleExceptionAsync(HttpContext context, Exception exception)
		{
			context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
			await context.Response.WriteAsync(exception.Message);
		}

	}
}
