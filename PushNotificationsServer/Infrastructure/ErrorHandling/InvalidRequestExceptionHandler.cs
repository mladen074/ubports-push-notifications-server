﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace PushNotificationsServer.Infrastructure.ErrorHandling
{
	public class InvalidRequestExceptionHandler : IExceptionHandler
	{
		private static readonly ISet<Type> HandledTypes = new HashSet<Type>
		{
			typeof(ArgumentNullException),
			typeof(ArgumentException),
		};

		public bool CanHandle(Type type) => HandledTypes.Contains(type);
		public bool StopProcessingOtherHandlers() => true;

		public async Task HandleExceptionAsync(HttpContext context, Exception exception)
		{
			if (context == null) throw new ArgumentNullException(nameof(context));
			if (exception == null) throw new ArgumentNullException(nameof(exception));

			context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
			await context.Response.WriteAsync(exception.Message);
		}
	}
}
