﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace PushNotificationsServer.Infrastructure.ErrorHandling
{
	public interface IExceptionHandler
	{
		bool CanHandle(Type type);
		bool StopProcessingOtherHandlers();
		Task HandleExceptionAsync(HttpContext context, Exception exception);
	}
}
