﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PushNotificationsServer.Extensions;
using PushNotificationsServer.Interfaces;
using PushNotificationsServer.RequestModels;
using System;
using System.Threading.Tasks;

namespace PushNotificationsServer.Controllers
{
	[ApiController]
	[Route("")]
	public class HomeController : ControllerBase
	{
		public HomeController(IQueueService queueService, IMapper mapper)
		{
			_queueService = queueService ?? throw new ArgumentNullException(nameof(queueService));
			_mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
		}

		private readonly IQueueService _queueService;
		private readonly IMapper _mapper;

		public async Task<IActionResult> Get()
		{
			var view = await GetType().GetStringResourceAsync("Views.Home.Get.cshtml");

			return new ContentResult
			{
				Content = view,
				ContentType = "text/html;charset=utf-8",
			};
		}

		[HttpPost("/register")]
		public async Task<IActionResult> Register(PushNotificationTargetRequestModel target)
		{
			ValidateTarget(target);
			await _queueService.Register(_mapper.Map<PushNotificationTarget>(target));

			return Ok();
		}

		[HttpPost("/unregister")]
		public async Task<IActionResult> Unregister(PushNotificationTargetRequestModel target)
		{
			ValidateTarget(target);
			await _queueService.Unregister(_mapper.Map<PushNotificationTarget>(target));

			return Ok();
		}

		private void ValidateTarget(PushNotificationTargetRequestModel target)
		{
			if (target == null) throw new ArgumentNullException(nameof(target));
			if (string.IsNullOrWhiteSpace(target.ApplicationId)) throw new ArgumentNullException(nameof(target.ApplicationId));
			if (string.IsNullOrWhiteSpace(target.DeviceId)) throw new ArgumentNullException(nameof(target.DeviceId));
		}

		[HttpPost("/notify")]
		public async Task<IActionResult> Notify(PushNotificationMessageRequestModel message)
		{
			ValidateMessage(message);
			await _queueService.PushAsync(_mapper.Map<PushNotificationMessage>(message));

			return Ok();
		}

		private void ValidateMessage(PushNotificationMessageRequestModel message)
		{
			if (message == null) throw new ArgumentNullException(nameof(message));
			if (string.IsNullOrWhiteSpace(message.ApplicationId)) throw new ArgumentNullException(nameof(message.ApplicationId));
			if (string.IsNullOrWhiteSpace(message.Token)) throw new ArgumentNullException(nameof(message.Token));
			if (message.ExpiresOn < DateTime.Now) throw new ArgumentException(nameof(message.ExpiresOn));
			if (string.IsNullOrWhiteSpace(message.Data)) throw new ArgumentNullException(nameof(message.Data));
		}
	}
}
