﻿using System.IO;
using System.Threading.Tasks;

namespace PushNotificationsServer.Extensions
{
	public static class StreamExtensions
	{
		public static string GetString(this Stream source)
		{
			using (var reader = new StreamReader(source))
			{
				return reader.ReadToEnd();
			}
		}

		public static async Task<string> GetStringAsync(this Stream source)
		{
			using (var reader = new StreamReader(source))
			{
				return await reader.ReadToEndAsync();
			}
		}
	}
}
