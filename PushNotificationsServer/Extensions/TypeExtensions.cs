﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace PushNotificationsServer.Extensions
{
	public static class TypeExtensions
	{
		public static async Task<string> GetStringResourceAsync(this Type source, string resourcePath)
		{
			if (source == null) throw new ArgumentNullException(nameof(source));
			if (resourcePath == null) throw new ArgumentNullException(nameof(resourcePath));

			var assemblyName = source.Assembly.GetName().Name;
			if (!resourcePath.StartsWith(assemblyName))
			{
				resourcePath = $"{assemblyName}.{resourcePath}";
			}

			using (var stream = source.Assembly.GetManifestResourceStream(resourcePath))
			{
				if (stream == null) throw new InvalidOperationException("The specified resource path is not valid");

				using (var reader = new StreamReader(stream))
				{
					return await reader.ReadToEndAsync();
				}
			}
		}
	}
}
