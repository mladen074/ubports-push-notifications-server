﻿using System;

namespace PushNotificationsServer.Extensions
{
	public static class DateTimeExtensions
	{
		public static string ToIso8601(this DateTime source)
		{
			return source.ToString("yyyy-MM-ddThh:mm:sszzz");
		}
	}
}
