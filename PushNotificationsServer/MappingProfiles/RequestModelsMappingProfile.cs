﻿using AutoMapper;
using PushNotificationsServer.Interfaces;
using PushNotificationsServer.RequestModels;

namespace PushNotificationsServer.MappingProfiles
{
	public class RequestModelsMappingProfile : Profile
	{
		public RequestModelsMappingProfile()
		{
			CreateMap<PushNotificationTargetRequestModel, PushNotificationTarget>();
			CreateMap<PushNotificationMessageRequestModel, PushNotificationMessage>();
		}
	}
}
