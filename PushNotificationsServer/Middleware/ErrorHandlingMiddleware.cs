﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PushNotificationsServer.Infrastructure.ErrorHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushNotificationsServer.Middleware
{
	public class ErrorHandlingMiddleware
	{
		public ErrorHandlingMiddleware(RequestDelegate nextDelegate, ILogger<ErrorHandlingMiddleware> logger)
		{
			_nextDelegate = nextDelegate;
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));

			EnumerateExceptionHandlers();
		}

		private readonly RequestDelegate _nextDelegate;
		private readonly ILogger<ErrorHandlingMiddleware> _logger;

		public async Task Invoke(HttpContext context)
		{
			try
			{
				if (_nextDelegate != null) await _nextDelegate(context);
			}
			catch (Exception exception)
			{
				_logger.LogError(exception, $"[{context.TraceIdentifier}]: Exception caught");

				var exceptionHandlers = GetExceptionHandlers(exception)
					.DefaultIfEmpty(new DefaultExceptionHandler())
					.OrderBy(h => h is DefaultExceptionHandler) // place it as last
					.ToArray();

				foreach (var handler in exceptionHandlers)
				{
					await handler.HandleExceptionAsync(context, exception);
					if (handler.StopProcessingOtherHandlers()) break;
				}
			}
		}

		private IReadOnlyCollection<IExceptionHandler> _exceptionHandlers;

		private IEnumerable<IExceptionHandler> GetExceptionHandlers(Exception exception)
		{
			return _exceptionHandlers
				.Where(h => h.CanHandle(exception.GetType()));
		}

		private void EnumerateExceptionHandlers()
		{
			_exceptionHandlers = GetType()
				.Assembly
				.GetTypes()
				.Where(t => typeof(IExceptionHandler).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract)
				.Select(t => (IExceptionHandler)Activator.CreateInstance(t))
				.ToArray();
		}
	}
}
