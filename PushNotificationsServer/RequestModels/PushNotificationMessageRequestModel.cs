﻿using System;
using System.Text.Json.Serialization;

namespace PushNotificationsServer.RequestModels
{
	public class PushNotificationMessageRequestModel : PushNotificationTargetRequestModel
	{
		[JsonPropertyName("expire_on")]
		public DateTime ExpiresOn { get; set; }

		[JsonPropertyName("token")]
		public string Token { get; set; }

		[JsonPropertyName("clear_pending")]
		public bool ShouldClearPending { get; set; }

		[JsonPropertyName("replace_tag")]
		public string ReplaceTag { get; set; }

		[JsonPropertyName("data")]
		public string Data { get; set; }
	}
}
