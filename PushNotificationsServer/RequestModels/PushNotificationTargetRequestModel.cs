﻿using System.Text.Json.Serialization;

namespace PushNotificationsServer.RequestModels
{
	public class PushNotificationTargetRequestModel
	{
		[JsonPropertyName("appid")]
		public string ApplicationId { get; set; }

		[JsonPropertyName("deviceid")]
		public string DeviceId { get; set; }
	}
}
