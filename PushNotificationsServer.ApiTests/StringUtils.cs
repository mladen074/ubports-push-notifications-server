﻿using System;

namespace PushNotificationsServer.ApiTests
{
	public static class StringUtils
	{
		public static string GetRandomString(uint maxLength)
		{
			var buffer = new byte[maxLength];
			var random = new Random();
			random.NextBytes(buffer);

			return Convert
				.ToBase64String(buffer)
				.Substring(0, (int)maxLength);
		}
	}
}
