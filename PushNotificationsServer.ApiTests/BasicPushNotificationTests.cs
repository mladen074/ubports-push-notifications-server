using Microsoft.VisualStudio.TestTools.UnitTesting;
using PushNotificationsServer.Extensions;
using System;
using System.Net;
using System.Threading.Tasks;
using PushNotificationsServerStartup = PushNotificationsServer.Startup;

namespace PushNotificationsServer.ApiTests
{
	[TestClass]
	public class BasicPushNotificationTests : IntegrationTests<PushNotificationsServerStartup>
	{
		[TestMethod]
		public async Task HomeGetTest()
		{
			var response = await GetAsync("/");
			response.EnsureSuccessStatusCode();

			var receivedContent = await response.Content.ReadAsStringAsync();
			var expectedContent = await StartupType.GetStringResourceAsync("Views.Home.Get.cshtml");

			Assert.AreEqual(expectedContent, receivedContent);
		}

		[TestMethod]
		public async Task InvalidPropertiesInRegisterTargetTest()
		{
			var url = "/register";

			await ExpectErrorBadRequest(url, new PushTarget());

			await ExpectErrorBadRequest(url, CreateValidPushTarget(t => { t.deviceid = null; }));
			await ExpectErrorBadRequest(url, CreateValidPushTarget(t => { t.deviceid = string.Empty; }));
			await ExpectErrorBadRequest(url, CreateValidPushTarget(t => { t.appid = null; }));
			await ExpectErrorBadRequest(url, CreateValidPushTarget(t => { t.appid = string.Empty; }));
		}

		[TestMethod]
		public async Task RegisterPushUnregisterTargetTest()
		{
			var message = CreateValidPushMessage();

			var registerResponse = await PostEntityAsync("/register", message);
			Assert.AreEqual(HttpStatusCode.OK, registerResponse.StatusCode);

			var notifyResponse = await PostEntityAsync("/notify", message);
			Assert.AreEqual(HttpStatusCode.OK, notifyResponse.StatusCode);

			var unregisterResponse = await PostEntityAsync("/unregister", message);
			Assert.AreEqual(HttpStatusCode.OK, unregisterResponse.StatusCode);
		}

		[TestMethod]
		public async Task InvalidPropertiesInUnregisterTargetTest()
		{
			var url = "/unregister";

			await ExpectErrorBadRequest(url, new PushTarget());

			await ExpectErrorBadRequest(url, CreateValidPushTarget(t => { t.deviceid = null; }));
			await ExpectErrorBadRequest(url, CreateValidPushTarget(t => { t.deviceid = string.Empty; }));
			await ExpectErrorBadRequest(url, CreateValidPushTarget(t => { t.appid = null; }));
			await ExpectErrorBadRequest(url, CreateValidPushTarget(t => { t.appid = string.Empty; }));
		}

		[TestMethod]
		public async Task InvalidPropertiesInNotificationTest()
		{
			var url = "/notify";

			await ExpectErrorBadRequest(url, new PushMessage());

			await ExpectErrorBadRequest(url, CreateValidPushMessage(msg => { msg.token = null; }));
			await ExpectErrorBadRequest(url, CreateValidPushMessage(msg => { msg.token = string.Empty; }));
			await ExpectErrorBadRequest(url, CreateValidPushMessage(msg => { msg.appid = null; }));
			await ExpectErrorBadRequest(url, CreateValidPushMessage(msg => { msg.appid = string.Empty; }));
			await ExpectErrorBadRequest(url, CreateValidPushMessage(msg => { msg.data = null; }));
			await ExpectErrorBadRequest(url, CreateValidPushMessage(msg => { msg.data = string.Empty; }));
			await ExpectErrorBadRequest(url, CreateValidPushMessage(msg => { msg.expire_on = DateTime.Now.AddMonths(-1); }));
		}

		private static PushMessage CreateValidPushMessage(Action<PushMessage> afterCreateAction = null)
		{
			var message = new PushMessage
			{
				token = GetValidToken(),
				appid = GetValidApplicationId(),
				deviceid = GetValidDeviceId(),
				data = GetValidData(),
				expire_on = DateTime.Now.AddMonths(1),
				replace_tag = string.Empty,
				//replace_tag = GetValidReplaceTag(),
				//clear_pending = true,
			};

			afterCreateAction?.Invoke(message);

			return message;
		}

		private static PushTarget CreateValidPushTarget(Action<PushTarget> afterCreateAction = null)
		{
			var message = new PushTarget
			{
				appid = GetValidApplicationId(),
				deviceid = GetValidDeviceId(),
			};

			afterCreateAction?.Invoke(message);

			return message;
		}

		private static string GetValidToken()
		{
			return StringUtils.GetRandomString(8);
		}

		private static string GetValidApplicationId()
		{
			return StringUtils.GetRandomString(8);
		}

		private static string GetValidDeviceId()
		{
			return StringUtils.GetRandomString(8);
		}

		private static string GetValidData()
		{
			return StringUtils.GetRandomString(8);
		}

		private static string GetValidReplaceTag()
		{
			return StringUtils.GetRandomString(8);
		}

		private async Task ExpectErrorBadRequest(string url, PushMessage msg)
		{
			var response = await PostEntityAsync(url, msg);
			Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
		}

		private async Task ExpectErrorBadRequest(string url, PushTarget target)
		{
			var response = await PostEntityAsync(url, target);
			Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
		}

		private class PushTarget
		{
			public string appid { get; set; }
			public string deviceid { get; set; }
		}

		private class PushMessage : PushTarget
		{
			public string token { get; set; }
			public string data { get; set; }
			public DateTime expire_on { get; set; }
			public string replace_tag { get; set; }
			public bool clear_pending { get; set; }
		}
	}
}
