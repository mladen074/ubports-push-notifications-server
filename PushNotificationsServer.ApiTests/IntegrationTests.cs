using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PushNotificationsServer.ApiTests
{
	public class IntegrationTests<TStartup> where TStartup : class
	{
		public IntegrationTests()
		{
			var webHostBuilder = WebHost
				.CreateDefaultBuilder()
				.UseStartup<TStartup>()
				.ConfigureAppConfiguration(builder =>
				{
					builder
						.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
						.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
						;
				});

			Server = new TestServer(webHostBuilder);
		}

		public async Task<HttpResponseMessage> GetAsync(string url)
		{
			return await Server
				.CreateClient()
				.GetAsync(url);
		}

		public async Task<HttpResponseMessage> PostAsync(string url, HttpContent content)
		{
			return await Server
				.CreateClient()
				.PostAsync(url, content);
		}

		public async Task<HttpResponseMessage> PostEntityAsync<TContent>(string url, TContent content)
		{
			var stringContent = new StringContent(
				JsonConvert.SerializeObject(content),
				Encoding.UTF8,
				"application/json");

			return await PostAsync(url, stringContent);
		}

		public TestServer Server { get; }
		public Type StartupType { get; } = typeof(TStartup);
	}
}
