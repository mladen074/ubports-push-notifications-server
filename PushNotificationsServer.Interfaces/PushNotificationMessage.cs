﻿using System;

namespace PushNotificationsServer.Interfaces
{
	public class PushNotificationMessage : PushNotificationTarget
	{
		public DateTime ExpiresOn { get; set; }
		public string Token { get; set; }
		public bool ShouldClearPending { get; set; }
		public string ReplaceTag { get; set; }
		public string Data { get; set; }
	}
}
