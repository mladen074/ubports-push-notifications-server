﻿using System.Threading.Tasks;

namespace PushNotificationsServer.Interfaces
{
	public interface IQueueService
	{
		Task PushAsync(PushNotificationMessage message);
		Task Register(PushNotificationTarget target);
		Task Unregister(PushNotificationTarget target);
	}
}
