﻿namespace PushNotificationsServer.Interfaces
{
	public class PushNotificationTarget
	{
		public string ApplicationId { get; set; }
		public string DeviceId { get; set; }
	}
}
