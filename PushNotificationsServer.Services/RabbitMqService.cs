﻿using PushNotificationsServer.Interfaces;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Threading.Tasks;

namespace PushNotificationsServer.Services
{
	// TODO: integrate with RabbitMQ
	public class RabbitMqService : IQueueService
	{
		public RabbitMqService(IConnectionFactory connectionFactory)
		{
			_connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
			_connection = _connectionFactory.CreateConnection();
		}

		private readonly IConnectionFactory _connectionFactory;
		private IConnection _connection;

		public async Task PushAsync(PushNotificationMessage message)
		{
			ValidateToken(message.Token);
			EnsureConnectionExists();
			using (var channel = _connection.CreateModel())
			{
				var target = $"{message.DeviceId}:{message.ApplicationId}";

				channel.QueueDeclare(
					queue: target,
					durable: true,
					exclusive: false,
					autoDelete: false,
					arguments: null);

				channel.BasicPublish(
					exchange: "",
					routingKey: target,
					basicProperties: null,
					mandatory :true,
					body: Encoding.UTF8.GetBytes(message.Data));
			}
			
			await Task.CompletedTask;
		}

		private void ValidateToken(string token)
		{
			// TODO implement this
		}

		public async Task Register(PushNotificationTarget target)
		{
			EnsureConnectionExists();
			using (var channel = _connection.CreateModel())
			{
				channel.QueueDeclare(
					$"{target.DeviceId}:{target.ApplicationId}",
					durable: true,
					exclusive: false,
					autoDelete: false,
					arguments: null);
			}

			await Task.CompletedTask;
		}

		public async Task Unregister(PushNotificationTarget target)
		{
			EnsureConnectionExists();
			using (var channel = _connection.CreateModel())
			{
				channel.QueueDelete(
					$"{target.DeviceId}:{target.ApplicationId}",
					ifUnused: false,
					ifEmpty: false);
			}

			await Task.CompletedTask;
		}

		private void EnsureConnectionExists()
		{
			if (!_connection.IsOpen)
			{
				_connection = _connectionFactory.CreateConnection();
			}
		}
	}
}
